
![alt text](https://i.imgur.com/PO4eitQ.png)

# About

This modpack is designed for those looking to get away from the bigger and bigger numbers, single block machines, and hours of microcrafting. Similar to Proton, by Vazkii, this pack is hyper-focused to provide a challenging, but rewarding progression.

# Goals

* Lightweight memory footprint (Only 51 Mods!)
* Challenging progression through Better With Mods
* Transport through use of rail networks
* Building through chisels and bits
* Community!

# Play

Play this pack on a dedicated server at mc.betterwithmods.com

Join us on discord at https://discord.betterwithmods.com

 

*Main Logo Created by /u/ThePhail, contact him at https://discord.gg/EmpmJp5*

# Releasing

Uses https://github.com/Gaz492/twitch-export-builder to do exporting on linux
