# Current Version - **1.16**

## 1.16
*Updated*
BetterWithAddons: 0.42 -> 0.43
BetterWithMods: 2.3.4 -> 2.3.6
Signals: 1.3.8-19 -> 1.3.10-21
## 1.15

*Updated*
AromaBackup: b103 -> b104
BetterWithMods: 2.3.2 -> 2.3.4
Chisel And Bits: 14.19 -> 14.20
Colytra: 1.0.4.3
ConnectedTexturesMod: 0.3.1.16 -> 0.3.2.18
CulinaryConstruct: 1.10 -> 1.11
JEI: 4.11.0.204 -> 4.11.0.206
ModTweaker: 4.0.12 -> 4.0.13
Traverse: 1.5.4-61 -> 1.5.5-61

## 1.14

*Added:*  
MouseScroller - Improves GUI Functionality  
SmartHud - Improves Compass/Clock Functionality  
LagGoggles - Allows better profiling of lag sources

*Updated:*  
BetterWithMods: 2.2.0.7 -> 2.3.2  
Chisel & Bits: 14.18 ->  14.19  
Random Patches: 1.4.1.0 -> 1.4.1.1  
Signals: 1.3.6-17 -> 1.3.8-19


*Changes:*

Enable HCHopper
Diamond Shield now requires diamond ingots
Disallow opening a backpack while wearing it


