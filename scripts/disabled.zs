import crafttweaker.item.IItemStack;

var disabled = [
    <extrarails:teleporting_rail>,
] as IItemStack[];

for i, item in disabled {
    mods.jei.JEI.removeAndHide(item);
}