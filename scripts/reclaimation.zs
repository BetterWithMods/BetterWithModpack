import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.betterwithmods.Crucible;
import mods.betterwithmods.Kiln;
import mods.betterwithmods.Cauldron;

var sfsIngot = <betterwithmods:material:14>;
var diamondIngot = <betterwithmods:material:45>;
var goldIngot = <minecraft:gold_ingot>;
var ironIngot = <minecraft:iron_ingot>;

#Extra Rails
mods.betterwithmods.Crucible.addStoked([<extrarails:locking_rail>],[ironIngot.withAmount(6)]);
mods.betterwithmods.Crucible.addStoked([<extrarails:direction_rail>.withAmount(2)],[ironIngot.withAmount(6)]);
mods.betterwithmods.Crucible.addStoked([<extrarails:teleporting_rail>],[ironIngot.withAmount(6)]);
mods.betterwithmods.Crucible.addStoked([<extrarails:comparator_rail>],[ironIngot.withAmount(6)]);

#Spartan Shields
mods.betterwithmods.Crucible.addStoked([<spartanshields:shield_basic_iron>],[ironIngot.withAmount(4)]);
mods.betterwithmods.Crucible.addStoked([<spartanshields:shield_basic_gold>],[<minecraft:gold_ingot>.withAmount(4)]);
mods.betterwithmods.Crucible.addStoked([<spartanshields:shield_basic_diamond>],[<betterwithmods:material:45>.withAmount(4)]);
mods.betterwithmods.Crucible.addStoked([<spartanshields:shield_basic_soulforged_steel>],[<betterwithmods:material:14>.withAmount(4)]);

#Wolf Armor
mods.betterwithmods.Cauldron.addStoked([<wolfarmor:leather_wolf_armor>], [<betterwithmods:material:12>.withAmount(5)]);
mods.betterwithmods.Crucible.addStoked([<wolfarmor:chainmail_wolf_armor>], [<minecraft:iron_nugget>.withAmount(24)]);
mods.betterwithmods.Crucible.addStoked([<wolfarmor:iron_wolf_armor>], [ironIngot.withAmount(5)]);
mods.betterwithmods.Crucible.addStoked([<wolfarmor:gold_wolf_armor>], [<minecraft:gold_ingot>.withAmount(5)]);
mods.betterwithmods.Crucible.addStoked([<wolfarmor:diamond_wolf_armor>], [diamondIngot.withAmount(5)]);

#Wearable Backpacks
mods.betterwithmods.Crucible.addStoked([<wearablebackpacks:backpack>], [<betterwithmods:material:12>.withAmount(7), goldIngot.withAmount(1)]);
