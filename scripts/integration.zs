import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.betterwithmods.Crucible;
import mods.betterwithmods.Kiln;
import mods.betterwithmods.Cauldron;
import mods.betterwithmods.Anvil;

var anyChest = <ore:chest>;
var stick = <ore:stickWood>;
var siding = <betterwithmods:siding_wood>;
var moulding = <betterwithmods:moulding_wood>;
var sfsIngot = <ore:ingotSoulforgedSteel>;

#General

#Storage Drawers
var basicDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:0>);
var oneByTwoDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:1>);
var twoByTwoDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:2>);
var oneByTwoHalfDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:3>);
var twoByTwoHalfDrawerRecipes = recipes.getRecipesFor(<storagedrawers:basicdrawers:4>);

#This is the easiest way to do this even if it is ugly
for iter, drawerRecipe in basicDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
    var siding = siding.withTag({texture: {Properties: {variant: drawerRecipe.output.tag.material}, Name: "minecraft:planks"}});
    recipes.addShaped(drawerRecipe.name, drawerRecipe.output, [[siding, siding, siding], [null, anyChest, null], [siding, siding, siding]]);
}

for iter, drawerRecipe in oneByTwoDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
    var siding = siding.withTag({texture: {Properties: {variant: drawerRecipe.output.tag.material}, Name: "minecraft:planks"}});
    recipes.addShaped(drawerRecipe.name, drawerRecipe.output, [[siding, anyChest, siding], [siding, siding, siding], [siding, anyChest, siding]]);
}

for iter, drawerRecipe in twoByTwoDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
    var siding = siding.withTag({texture: {Properties: {variant: drawerRecipe.output.tag.material}, Name: "minecraft:planks"}});
    recipes.addShaped(drawerRecipe.name, drawerRecipe.output, [[anyChest, siding, anyChest], [siding, siding, siding], [anyChest, siding, anyChest]]);
}

for iter, drawerRecipe in oneByTwoHalfDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
    var moulding = moulding.withTag({texture: {Properties: {variant: drawerRecipe.output.tag.material}, Name: "minecraft:planks"}});
    recipes.addShaped(drawerRecipe.name, drawerRecipe.output, [[moulding, anyChest, moulding], [moulding, moulding, moulding], [moulding, anyChest, moulding]]);
}

for iter, drawerRecipe in twoByTwoHalfDrawerRecipes {
    recipes.remove(drawerRecipe.output, true);
    var moulding = moulding.withTag({texture: {Properties: {variant: drawerRecipe.output.tag.material}, Name: "minecraft:planks"}});
    recipes.addShaped(drawerRecipe.name, drawerRecipe.output, [[anyChest, moulding, anyChest], [moulding, moulding, moulding], [anyChest, moulding, anyChest]]);
}

var sidings = [
siding.withTag({texture: {Properties: {variant: "oak"}, Name: "minecraft:planks"}}),
siding.withTag({texture: {Properties: {variant: "dark_oak"}, Name: "minecraft:planks"}}),
siding.withTag({texture: {Properties: {variant: "birch"}, Name: "minecraft:planks"}}),
siding.withTag({texture: {Properties: {variant: "spruce"}, Name: "minecraft:planks"}}),
siding.withTag({texture: {Properties: {variant: "jungle"}, Name: "minecraft:planks"}}),
siding.withTag({texture: {Properties: {variant: "acacia"}, Name: "minecraft:planks"}}),
] as IItemStack[];

recipes.remove(<storagedrawers:trim:*>);
var trim = <storagedrawers:trim>.definition;

for meta, siding in sidings {
    recipes.addShaped("storagedrawers_trim" + meta, trim.makeStack(meta), [[stick, siding, stick], [siding, siding, siding], [stick, siding, stick]]);
}
recipes.remove(<storagedrawers:controller>);
mods.betterwithmods.Anvil.addShaped(<storagedrawers:controller>, [
    [<minecraft:stone:*>, <storagedrawers:trim:*>, <storagedrawers:trim:*>, sfsIngot],
    [<minecraft:stone:*>, <minecraft:ender_eye>, <betterwithmods:material:34>, sfsIngot],
    [<minecraft:stone:*>, <minecraft:ender_eye>, <betterwithmods:material:34>, sfsIngot],
    [<minecraft:stone:*>, <storagedrawers:trim:*>, <storagedrawers:trim:*>, sfsIngot]
]);

var chainMail = <betterwithmods:material:47>;
recipes.remove(<wolfarmor:chainmail_wolf_armor>);
recipes.addShaped("wolfarmor_chainmail", <wolfarmor:chainmail_wolf_armor>, [
    [chainMail, null, null],
    [chainMail, chainMail, chainMail],
    [chainMail, null, chainMail]
]);

#Spartan Shields

var diamondIngot = <betterwithmods:material:45>;

recipes.remove(<spartanshields:shield_basic_diamond>);
recipes.addShaped("spartanshield_diamondshield", <spartanshields:shield_basic_diamond>, [
    [null, diamondIngot, null],
    [diamondIngot, <spartanshields:shield_basic_wood>, diamondIngot],
    [null, diamondIngot, null]
]);